use tracing::Level;
use lightswitch::make_app;

#[cfg(debug_assertions)]
pub fn tracing_subscriber_setup() {
    tracing_subscriber::fmt()
        .with_max_level(Level::DEBUG)
        .init();
}

#[cfg(not(debug_assertions))]
pub fn tracing_subscriber_setup() {
    tracing_subscriber::fmt()
        .with_max_level(Level::ERROR)
        .init();
}

#[tokio::main]
async fn main() {
    tracing_subscriber_setup();

    let listener = tokio::net::TcpListener::bind("0.0.0.0:7070").await.unwrap();
    tracing::info!("Listening on 0.0.0.0:7070");
    
    let app = make_app();

    axum::serve(listener, app).await.unwrap();
}