use std::sync::{Arc, RwLock};

use axum::Router;
use axum::routing::{get, put};
use tower_http::trace::TraceLayer;

use crate::toggles::Toggles;

mod api;
pub mod toggles;

pub fn make_app() -> Router {
    let toggles = Arc::new(RwLock::new(Toggles::default()));
    make_router(toggles)
}

fn make_router(toggles: Arc<RwLock<Toggles>>) -> Router {
    let trace_layer = TraceLayer::new_for_http();

    Router::new()
        .route("/health", get(api::health))
        .route("/toggles/:name", get(api::get_toggle))
        .route("/toggles/:name", put(api::set_toggle).patch(api::set_toggle))
        .with_state(toggles)
        .layer(trace_layer)
}

#[cfg(test)]
mod tests {
    use axum::body::Body;
    use axum::http;
    use axum::http::{Request, StatusCode};
    use serde_json::json;
    use tower::{Service, ServiceExt};

    use crate::toggles::Toggles;

    use super::*;

    #[tokio::test]
    async fn test_health_is_ok() {
        let toggles = Arc::new(RwLock::new(Toggles::default()));
        let app = make_router(toggles);

        let response = app.oneshot(
            Request::builder().uri("/health").body(Body::empty()).unwrap()
        ).await.unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn test_getting_non_existing_toggle_returns_404() {
        let toggles = Arc::new(RwLock::new(Toggles::default()));
        let app = make_router(toggles);

        let response = app.oneshot(
            Request::builder().uri("/toggles/test").body(Body::empty()).unwrap()
        ).await.unwrap();

        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn test_setting_toggle_and_getting_it_works() {
        let toggles = Arc::new(RwLock::new(Toggles::default()));
        let mut router = make_router(toggles).into_service();
        let app = router.ready().await.unwrap();

        let response = &app.call(
            Request::builder()
                .method(http::Method::PUT)
                .uri("/toggles/test")
                .header(http::header::CONTENT_TYPE, mime::APPLICATION_JSON.as_ref())
                .body(Body::from(serde_json::to_string(&json!({"on": true})).unwrap()))
                .unwrap()
        ).await.unwrap();

        assert_eq!(response.status(), StatusCode::OK);

        let response = &app.call(
            Request::builder().uri("/toggles/test").body(Body::empty()).unwrap()
        ).await
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }
}