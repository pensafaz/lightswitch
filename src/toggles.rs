use std::collections::HashMap;

#[derive(Debug, Default)]
pub struct Toggles {
    toggles: HashMap<String, bool>,
}

impl Toggles {
    pub fn get(&self, name: &str) -> Option<bool> {
        self.toggles.get(name).cloned()
    }
    
    pub fn set(&mut self, name: &str, state: bool) {
        self.toggles.insert(name.to_string(), state);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_getting_non_existing_toggle_returns_none() {
        let toggles = Toggles::default();
        let result = toggles.get("some_toggle");
        assert_eq!(result, None);
    }

    #[test]
    fn test_setting_and_returning_a_toggle_works() {
        let mut toggles = Toggles::default();
        toggles.set("some_toggle", true);
        let result = toggles.get("some_toggle");
        assert_eq!(result, Some(true));
    }

    #[test]
    fn test_overriding_a_toggle_works() {
        let mut toggles = Toggles::default();
        toggles.set("some_toggle", true);
        toggles.set("some_toggle", false);
        let result = toggles.get("some_toggle");
        assert_eq!(result, Some(false));
    }
}