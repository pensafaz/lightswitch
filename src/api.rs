use axum::extract::{Path, State};
use std::sync::{Arc, RwLock};
use axum::Json;
use axum::http::StatusCode;
use serde::{Deserialize, Serialize};
use crate::toggles::Toggles;

pub async fn health() -> &'static str {
    "OK"
}

#[derive(Debug, Serialize)]
pub struct ToggleResponse {
    on: bool,
    name: String,
}

pub async fn get_toggle(
    Path(toggle_name): Path<String>,
    State(toggles): State<Arc<RwLock<Toggles>>>,
) -> Result<Json<ToggleResponse>, StatusCode> {
    let toggle = {
        let toggles = toggles.read().expect("Mutex was poisoned");
        toggles.get(&toggle_name)
    };
    toggle.map(|toggle| Json(ToggleResponse {
        on: toggle,
        name: toggle_name,
    })).ok_or(StatusCode::NOT_FOUND)
}

#[derive(Debug, Deserialize)]
pub struct ToggleRequest {
    on: bool,
}

pub async fn set_toggle(
    Path(toggle_name): Path<String>,
    State(toggles): State<Arc<RwLock<Toggles>>>,
    Json(request): Json<ToggleRequest>,
) -> Result<Json<ToggleResponse>, StatusCode> {
    {
        let mut toggles = toggles.write().expect("Mutex was poisoned");
        toggles.set(&toggle_name, request.on);
    }
    Ok(Json(ToggleResponse {
        on: request.on,
        name: toggle_name,
    }))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_getting_non_existing_toggle_returns_404() {
        let toggles = Arc::new(RwLock::new(Toggles::default()));
        let result = get_toggle(Path("non-existing".to_string()), State(toggles)).await;
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn test_getting_existing_toggle_returns_it() {
        let mut toggles = Toggles::default();
        toggles.set("tog", true);
        let toggles = Arc::new(RwLock::new(toggles));
        let result = get_toggle(Path("tog".to_string()), State(toggles)).await;
        assert!(&result.is_ok());
        let result = result.unwrap();
        assert_eq!(&result.name, "tog");
        assert!(&result.on);
    }

    // test setting toggle creates it
    
    // test setting toggle overrides existing
}
