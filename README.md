# Lightswitch

Lightswitch is a feature toggle server.

## Usage

### Running it

You can just start it to run with defaults (7070 port, no authentication, sqlite database at current dir):

> lightswitch

Or you can pass command line options for:

    --port
    --dbfile

### Client usage

Query a toggle:

> curl http://localhost:7070/toggles/NAME

    {
        "on": true,
        "name": "NAME",
        "last-changed": "timestamp",
    }

Update a toggle:

> curl -X PATCH http://localhost:7070/toggles/NAME -d '{"on": false}'

    200
    {
        "on": false,
        "name": "NAME",
        "last-changed": "timestamp",
    }