use lightswitch::make_app;

pub async fn spawn_app() -> u16 {
    let listener = tokio::net::TcpListener::bind("0.0.0.0:0").await.unwrap();
    let socket_addr = listener.local_addr().expect("Bound listener should return addr");

    drop(tokio::spawn(async move {
        let app = make_app();
        axum::serve(listener, app).await.expect("Should run serve without errors");
    }));
    socket_addr.port()
}