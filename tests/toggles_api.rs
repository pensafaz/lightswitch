use reqwest::StatusCode;
use serde_json::{json, Value};
use crate::common::spawn_app;

mod common;

#[tokio::test]
async fn test_get_non_existing_toggle() {
    let port = spawn_app().await;
    
    let client = reqwest::Client::new();

    let response = client
        .get(format!("http://localhost:{}/toggles/test", port))
        .send()
        .await
        .expect("Failed to execute request");

    assert_eq!(response.status(), StatusCode::NOT_FOUND);
}

#[tokio::test]
async fn test_setting_and_getting_toggle() {
    let port = spawn_app().await;
    let client = reqwest::Client::new();

    let response = client
        .put(format!("http://localhost:{}/toggles/test", port))
        .header("Content-type", "application/json")
        .body(serde_json::to_string(&json!({
            "on": true
        })).unwrap())
        .send()
        .await
        .expect("Failed to execute request");

    assert_eq!(response.status(), StatusCode::OK);

    let response = client
        .get(format!("http://localhost:{}/toggles/test", port))
        .send()
        .await
        .expect("Failed to execute request");

    assert_eq!(response.status(), StatusCode::OK);
    let response: Value = response.json().await.unwrap();   
    assert!(response.get("on").unwrap().as_bool().unwrap());
}