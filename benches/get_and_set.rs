use criterion::{black_box, Criterion, criterion_group, criterion_main};
use lightswitch::toggles::Toggles;

fn get_and_set(n: usize, t: &mut Toggles) {
    for i in 0..n {
        t.set("test", i % 2 == 0);
        for _ in 0..10 {
            t.get("test");
        }
    }
}

fn criterion_benchmark(c: &mut Criterion) {
    let mut t = Toggles::default();

    for i in 0..10000 {
        let name = format!("test{}", i);
        t.set(&name, i % 2 == 0);
    }
    c.bench_function("get_and_set", |b| b.iter(|| {
        get_and_set(black_box(10000), &mut t)
    }));
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);