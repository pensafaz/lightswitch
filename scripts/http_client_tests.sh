#!/bin/bash

cargo run --release &

i=0

until [ "$(curl -s http://localhost:7070/health)" ]
do
  ((i++))
  echo "waiting $i times"
  if (( i > 20 ))
  then
    echo 'Could not start server in expected time'
    exit 1
  fi
  sleep 1
done

ijhttp tests/tests.http
kill %1